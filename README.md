# Technical assessment - Symfony 


## Run Locally

1. If not already done, [install Docker Compose](https://docs.docker.com/compose/install/) (v2.10+)
2. Clone the project

 ``` bash
  git clone https://gitlab.com/hbl-technical-assessment/symfony.git
 ```

3. Build fresh images
 ``` bash
docker compose build --no-cache
 ```
4. Start the project

``` bash
docker compose up --pull --wait
````

5. Ensure that all composer dependencies are installed
 ``` bash
docker compose exec php composer install
 ```
6. Run migration 
 ``` bash
docker compose exec php bin/console make:migration
 ```
``` bash
docker compose exec php bin/console doctrine:migrations:migrate
```
7. Load fixtures 
 ``` bash
docker compose exec php bin/console doctrine:fixtures:load
```
8. Open [https://localhost/shipped/orderdetails](https://localhost/shipped/orderdetails) in your web browser and accept the auto-generated TLS certificate to see the list of orderdetails 

9. Open [https://localhost/order/with-product-code?productCode=1234560](https://localhost/order/with-product-code?productCode=1234560) in your web browser and accept the auto-generated TLS certificate to see the list of order with a specific product code 

8. Run `docker compose down --remove-orphans` to stop the Docker containers.
