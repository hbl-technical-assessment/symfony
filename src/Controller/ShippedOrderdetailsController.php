<?php

namespace App\Controller;

use App\Repository\OrderdetailRepository;
use App\Repository\OrderRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;

class ShippedOrderdetailsController extends AbstractController
{
    #[Route('/shipped/orderdetails', name: 'app_shipped_orderdetails')]
    public function index(OrderdetailRepository $orderdetailRepository, OrderRepository $orderRepository): JsonResponse
    {
        $orderdetails = $orderdetailRepository->findAll();
        
        $response = [];
        foreach($orderdetails as $orderdetail) {
            if ($orderdetail->getRelatedOrder()->getShippedDate() === null) continue;
            $relatedOrder = $orderdetail->getRelatedOrder();
            $product = $orderdetail->getProduct();   
            $response[] = [
                'orderNumber' => $relatedOrder->getOrderNumber(),
                'orderDate' => $relatedOrder->getOrderDate(),
                'orderShippedDate' => $relatedOrder->getShippedDate(),
                'productName' => $product->getName(),
                'productCode' => $product->getCode(),
                'productDescription' => $product->getDescription(),
            ];
        }
        
        return $this->json([
            'response' => $response,
        ]);
    }
}
