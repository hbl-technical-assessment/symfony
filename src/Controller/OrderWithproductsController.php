<?php

namespace App\Controller;

use App\Repository\OrderRepository;
use App\Repository\OrderdetailRepository;
use App\Repository\ProductRepository;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class OrderWithproductsController extends AbstractController
{
    #[Route('/order/with-product-code', name: 'app_order_withproducts', methods: ['GET'])]
    public function index(Request $request, OrderdetailRepository $orderdetailRepository, ProductRepository $productRepository): JsonResponse
    {
        $productCode = $request->query->get('productCode');
        if (!$productCode) {
            return $this->json(['error' => 'Product code is required.'], 400);
        }
        
        $product = $productRepository->findOneBy(['code' => $request->query->get('productCode')]);
        if (!$product) {
            return $this->json(['error' => 'Product not found.'], 404);
        }
        
        $orderdetails = $orderdetailRepository->findBy(['product' => $product]);
        $response = [];
        foreach($orderdetails as $orderdetail) {
            $order = $orderdetail->getRelatedOrder();
            if(!$order) continue;
            $response[] = $order->getOrderNumber();
            
        }

        return $this->json([
            'response' => $response,
        ]);
    }
}
