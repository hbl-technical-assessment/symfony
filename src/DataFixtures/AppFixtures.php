<?php

namespace App\DataFixtures;

use App\Entity\Order;
use App\Entity\Product;
use App\Entity\Orderdetail;
use Doctrine\Persistence\ObjectManager;
use Doctrine\Bundle\FixturesBundle\Fixture;

class AppFixtures extends Fixture
{
    public function load(ObjectManager $manager): void
    {
        for ($i = 0; $i < 10; $i++) {
            // Create a product.
            $product = new Product();
            $product->setName('Product ' . $i);
            $product->setCode('123456' . $i);
            $product->setDescription('This is a product.');
            $manager->persist($product);
        
            // Create an order.
            $order = new Order();
            $order->setOrderNumber($i + 1);
            $order->setOrderDate(new \DateTime());
            $order->setShippedDate(new \DateTime());
            $manager->persist($order);
        
            // Create an orderdetail for the product and the order.
            $orderdetail = new Orderdetail();
            $orderdetail->setLineNumber($i + 1);
            $orderdetail->setRelatedOrder($order);
            $orderdetail->setProduct($product);
            $manager->persist($orderdetail);

            $manager->flush();
        }
    }
}
