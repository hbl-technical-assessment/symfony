<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20230823213538 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE orderdetail DROP CONSTRAINT fk_27a0e7f22b1c2395');
        $this->addSql('DROP INDEX uniq_27a0e7f22b1c2395');
        $this->addSql('ALTER TABLE orderdetail RENAME COLUMN related_order_id TO product_id');
        $this->addSql('ALTER TABLE orderdetail ADD CONSTRAINT FK_27A0E7F24584665A FOREIGN KEY (product_id) REFERENCES product (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_27A0E7F24584665A ON orderdetail (product_id)');
        $this->addSql('ALTER TABLE product DROP CONSTRAINT fk_d34a04ad17e8a46a');
        $this->addSql('DROP INDEX idx_d34a04ad17e8a46a');
        $this->addSql('ALTER TABLE product DROP orderdetail_id');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE SCHEMA public');
        $this->addSql('ALTER TABLE orderdetail DROP CONSTRAINT FK_27A0E7F24584665A');
        $this->addSql('DROP INDEX UNIQ_27A0E7F24584665A');
        $this->addSql('ALTER TABLE orderdetail RENAME COLUMN product_id TO related_order_id');
        $this->addSql('ALTER TABLE orderdetail ADD CONSTRAINT fk_27a0e7f22b1c2395 FOREIGN KEY (related_order_id) REFERENCES "order" (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('CREATE UNIQUE INDEX uniq_27a0e7f22b1c2395 ON orderdetail (related_order_id)');
        $this->addSql('ALTER TABLE product ADD orderdetail_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE product ADD CONSTRAINT fk_d34a04ad17e8a46a FOREIGN KEY (orderdetail_id) REFERENCES orderdetail (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('CREATE INDEX idx_d34a04ad17e8a46a ON product (orderdetail_id)');
    }
}
