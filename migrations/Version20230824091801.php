<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20230824091801 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE orderdetail ADD related_order_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE orderdetail ADD CONSTRAINT FK_27A0E7F22B1C2395 FOREIGN KEY (related_order_id) REFERENCES "order" (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_27A0E7F22B1C2395 ON orderdetail (related_order_id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE SCHEMA public');
        $this->addSql('ALTER TABLE orderdetail DROP CONSTRAINT FK_27A0E7F22B1C2395');
        $this->addSql('DROP INDEX UNIQ_27A0E7F22B1C2395');
        $this->addSql('ALTER TABLE orderdetail DROP related_order_id');
    }
}
